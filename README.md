# Patch and build script for running KiCad under native Wayland

Install the [required dependencies for your distro](https://dev-docs.kicad.org/en/build/linux/#_dependencies), as well as `g++`, `cmake`, `ninja` and `mold`.

Note: If your GCC version is older than 12.1.0, you must remove `-DCMAKE_CXX_FLAGS=-fuse-ld=mold` from the build script (also, you don't need to install `mold`).
