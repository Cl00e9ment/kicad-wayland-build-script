#!/bin/bash

KICAD_VERSION=7.0.5

set -e

git clone --depth 1 --branch "$KICAD_VERSION" https://gitlab.com/kicad/code/kicad.git
patch -p0 -d kicad < make-kicad-work-in-native-wayland.patch
mkdir -p kicad/build/release
cd kicad/build/release
cmake -G Ninja -DCMAKE_BUILD_TYPE=RelWithDebInfo -DKICAD_USE_EGL=on -DCMAKE_CXX_FLAGS=-fuse-ld=mold ../..
ninja

KICAD_RUN_FROM_BUILD_DIR=1 ./kicad/kicad
